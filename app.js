const express = require('express')
const { Pool, Client } = require('pg')

const client = new Client({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: 'postgres',
  port: 5432,
})
client.connect()

const app = express()
const port = 8000

app.use(express.json())

var to_str_query = (data) => { return `'${data}'` }

app.post('/home', (request, response) => {
  var name = request.body.name
  var desc = request.body.desc
  var price = request.body.price
  var post_code = request.body.post_code

  client.query(`INSERT INTO property (name, description, price, post_code) VALUES (${to_str_query(name)}, ${to_str_query(desc)}, ${price}, ${to_str_query(post_code)})`, (error, results) => {
    if (error) {
      response.send({
        status: 'Fail',
        message: error,
        data: null
      })
    }
    response.send({
      status: 'Success',
      message: null,
      data: null
    })
  })
})

app.get('/home', (request, response) => {
  var skip = request.query.skip
  var take = request.query.take
  client.query(`SELECT id, name, description as desc, price, post_code FROM property LIMIT ${take} OFFSET ${skip}`, (error, results) => {
    console.log(error, results)
    if (error) {
      response.send({
        status: 'Fail',
        message: error,
        data: null
      })
    }
    response.send({
      status: 'Success',
      message: null,
      data: {
        payload: results.rows,
        count: results.rowCount
      }
    })
  })
})

app.get('/postCode', (request, response) => {
  client.query(`SELECT post_code FROM property`, (error, results) => {
    console.log(error, results)
    if (error) {
      response.send({
        status: 'Fail',
        message: error,
        data: null
      })
    }
    response.send({
      status: 'Success',
      message: null,
      data: {
        payload: results.rows,
        count: results.rowCount
      }
    })
  })
})

app.get('/postCode/:id', (request, response) => {
  var post_code = request.params.id
  client.query(`SELECT id, name, description as desc, price, post_code FROM property WHERE post_code=${to_str_query(post_code)} ORDER BY price`, (error, results) => {
    if (error) {
      response.send({
        status: 'Fail',
        message: error,
        data: null
      })
      return
    }
    if ( results.rowCount == 0 ) {
      response.send({
        status: 'Success',
        message: null,
        data: {
          average: null,
          median: null
        }
      })
      return
    }
    var median = null
    if (results.rowCount % 2 == 0) {
      median = ( parseFloat(results.rows[ results.rowCount / 2 ].price) + parseFloat(results.rows[ results.rowCount / 2 - 1 ].price) )  / 2
    } else {
      median = parseFloat( results.rows[ parseInt(results.rowCount / 2) ].price )
    }
    var average = 0
    for (var i = 0; i < results.rows.length; i++) {
      average += parseFloat(results.rows[i].price)
    }
    average = average / results.rowCount
    response.send({
      status: 'Success',
      message: null,
      data: {
        average: average,
        median: median
      }
    })
  })
})

app.listen(port)
